package com.springandvuejs.springbootbackend.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.springandvuejs.springbootbackend.entity.Employee;

public interface EmployeeRepo extends JpaRepository<Employee, Long> {
	
	
 
}
