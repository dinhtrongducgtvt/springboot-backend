package com.springandvuejs.springbootbackend.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springandvuejs.springbootbackend.entity.Employee;
import com.springandvuejs.springbootbackend.repo.EmployeeRepo;

@RestController
@RequestMapping("/api")
@CrossOrigin("http://localhost:8081/")
public class EmployController {
	@Autowired
	private EmployeeRepo employeeRepo;

	@GetMapping("/employees")
	public List<Employee> fetchEmployees() {
		return employeeRepo.findAll();
	}
}
