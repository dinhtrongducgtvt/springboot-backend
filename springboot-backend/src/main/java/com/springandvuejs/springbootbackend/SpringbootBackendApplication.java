package com.springandvuejs.springbootbackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.springandvuejs.springbootbackend.entity.Employee;
import com.springandvuejs.springbootbackend.repo.EmployeeRepo;

@SpringBootApplication
public class SpringbootBackendApplication implements CommandLineRunner{
	@Autowired
	private EmployeeRepo employeeRepo;

	public static void main(String[] args) {
		SpringApplication.run(SpringbootBackendApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Employee employee1 = Employee.builder()
				.firstName("Nguyen Thi")
				.lastName("Mai")
				.email("nguyenthimai@gmail.com")
				.build();
		Employee employee2 = Employee.builder()
				.firstName("Hoang Van")
				.lastName("The")
				.email("hoangvanthe@gmail.com")
				.build();
		Employee employee3 = Employee.builder()
				.firstName("Dinh Trong")
				.lastName("Duc")
				.email("dinhtrongduc@gmail.com")
				.build();
		
		
		employeeRepo.save(employee1);
		employeeRepo.save(employee2);
		employeeRepo.save(employee3);
		
	}

}
